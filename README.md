# Hardware-CI-Demo
- STM32F401 Nucleo Development Board
- Sigrok-supported logic analyzer (fx2)

![MCU and logical analyzer](MCUandLogicAnalyzer.jpg)

# Runner Environment Setup

## Microcontroller programmer software 
- plug in the st-link and verify that its connected using lsusb
- sudo apt install stlink-tools
- sudo udevadm control --reload-rules
- sudo udevadm trigger
- st-util to verify that your st-link programmer can be found

## Logic Analyzer software
- sudo apt install sigrok
- sigrok-cli --scan to validate that your logic analyzer can be found

## GitLab Runner 
- sudo apt install curl
- curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
- sudo apt-get install gitlab-runner
- sudo gitlab-runner register
- sudo adduser gitlab-runner plugdev

## UART Tester Dependencies
- sudo pip3 install pyserial
.
