import sys
import serial
import argparse

parser = argparse.ArgumentParser(description='UART Test Helper')
parser.add_argument('--device', required=True, type=str)
parser.add_argument('--send', required=True, type=str)
parser.add_argument('--recieve', required=True, type=str)
args = parser.parse_args()

with serial.Serial(args.device, 115200, timeout=1) as ser:
    ser.write(args.send.encode())
    s = ser.read(50)        # read up to ten bytes (timeout)
    print("Response", end = " ")
    print(s)

if s == args.recieve.encode() :
    print("UART Test Passed.")
    sys.exit(0)
else:
    print("UART Test Failed.")
    sys.exit(1)


    