import sys

for line in sys.stdin:
    val = line.split()
    val_float = float(val[1].strip('%'))
    print(val[1])
    if (val_float < 47.0) or (val_float > 53.0) :
        print("Out of range PWM value.")
        sys.exit(1) 
    
sys.exit(0)